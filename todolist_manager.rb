class TodolistManager
  def initialize(filename)
    @filename = filename
    create_file unless File.exist?(@filename)
    load_tasks
  end

  def create_file
    File.open(@filename, 'w') {}
  end

  def load_tasks
    @tasks = File.readlines(@filename).map(&:chomp)
  end
  
  def save_tasks
    File.open(@filename, 'w') { |file| file.puts @tasks }
  end
  
  def list_tasks
    puts ""
    puts "Your To-Do List:"
    if @tasks.length == 0
      puts "Empty"
    else
      @tasks.each_with_index { |task, index| puts "#{index + 1}. #{task}" }
    end
  end

  def add_task(task)
    @tasks << task
    save_tasks
    puts ""
    puts "Task added: #{task}"
  end

  def remove_task(index)
    if index >= 1 && index <= @tasks.length
      removed_task = @tasks.delete_at(index - 1)
      save_tasks
      puts ""
      puts "Task removed: #{removed_task}"
    else
      puts ""
      puts "Invalid index. Please provide a valid task index."
    end
  end
end
