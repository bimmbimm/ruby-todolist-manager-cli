require_relative 'todolist_manager'

filename = 'todo.txt'
todo_manager = TodolistManager.new(filename)

loop do
  puts ""
  puts "Options:"
  puts "1. List tasks"
  puts "2. Add task"
  puts "3. Remove task"
  puts "4. Exit"

  print "Select an option: "
  choice = gets.chomp.to_i

  case choice
  when 1
    todo_manager.list_tasks
  when 2
    print "Enter task: "
    task = gets.chomp
    todo_manager.add_task(task)
  when 3
    print "Enter task index to remove: "
    index = gets.chomp.to_i
    todo_manager.remove_task(index)
  when 4
    puts ""
    puts "Exiting. Goodbye!"
    break
  else
    puts ""
    puts "Invalid choice. Please select a valid option."
  end
end
